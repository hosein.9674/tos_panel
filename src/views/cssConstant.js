import {css} from '@emotion/core';

export const override = css`
  position: fixed;
  margin: 0 auto;
  border-color: red;
  z-index:10000 !important;
  top: 0;
 
  left: 0;
  bottom: 56px;
  right: 0;
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

