import React, {Component} from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row
} from 'reactstrap';
import {PulseLoader} from "react-spinners";
import * as cssConstant from "../cssConstant";
import StatisticsService from "../../services/StatisticsService";
import Statistics from "../../views/StatisticChart"
import 'bootstrap/dist/css/bootstrap.min.css';
import {getErrorMessage} from  '../../views/consts'



export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.StatisticsService = new StatisticsService();
   
    this.state = {
     messages:[],
      showErrors:false,
    };
  }

  componentDidMount(){
    this.StatisticsService.getStatistics().then(res => {
      this.setState({
        loading: true
    });
        let labels=[];
        let datasets=[];
        let backgroundColor=[];
        for(let key of res.data.items){
            labels.push( key.course_name )
        }
        for(let key of res.data.items){
          datasets.push( key.count )
        }
        
        for(let i=0 ; i< datasets.length ; i++){
          backgroundColor.push( `#d${i}${9-i}f${i}d` )
        }
        
        this.setState({
            labels: labels,
            backgroundColor:backgroundColor,
            hoverBackgroundColor:backgroundColor,
            data:datasets,
            loading: false
        });
      
      }).catch(error => {
        this.setState({loading: false,});
        this.setState({showErrors: true, messages:getErrorMessage(error)});
      });
     
    }



  // loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {
    const {data,labels,backgroundColor,hoverBackgroundColor} = this.state;
    return (
      <div className="animated fadeIn">
        <Row className='dashboardCardStyle'> 
          <Col>
            <Card className='dashboard'>
              <CardHeader >
                تعداد سوال ها
              </CardHeader>
              <CardBody>
              <div className='sweet-loading'>
          <PulseLoader
            css={cssConstant.override}
            sizeUnit={"px"}
            size={25}
            color={'#20a8d8'}
            loading={this.state.loading}
          />
        </div>
                <div className="chart-wrapper">
                  {(labels && data && backgroundColor && hoverBackgroundColor) ? <Statistics labels={labels} hoverBackgroundColor={hoverBackgroundColor} backgroundColor={backgroundColor} chartData={data}/> :""}
               
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
     
      </div>
    );
  }

}


