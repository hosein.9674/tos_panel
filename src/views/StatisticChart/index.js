import React, { Component } from 'react';
import { Pie ,defaults } from 'react-chartjs-2';
import { Card, CardBody } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.scss';


class Statistics extends Component {
    constructor(props) {

        super(props);
      
        this.state = {
        
            chart: {
                labels: this.props.labels,
                  datasets: [
                    {
                      data:this.props.chartData,
                      backgroundColor:this.props.backgroundColor,
                      hoverBackgroundColor:this.props.hoverBackgroundColor,
                    
                    }],
            },
         
        };
        
       
       
      }
      
   
  render() {
    defaults.global.defaultFontFamily = 'Shabnam_Bold'
    return (
      <div className="animated fadeIn">
         
        <Card>
        
            <CardBody>
              <div className="chart-wrapper">
                <Pie data={this.state.chart}  
                options={{maintainAspectRatio: false}}  defaults={defaults.global.defaultFontFamily}/>
              </div>
            </CardBody>
          </Card>
      </div>
    );
  }
}

export default Statistics;
