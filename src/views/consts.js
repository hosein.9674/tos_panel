
export const ApiUrl = "http://tops.mtamadon.ir/api/v1";
export const Login = ApiUrl + "/auth/login";

export const TOKEN = ApiUrl + "TOPS_TOKEN";


export const Next = "صفحه قبلی";
export const Previous = "صفحه بعدی";



export function getErrorMessage(error) {
  let message = 'System Error';

  if (error === undefined || error.response === undefined ||
    error.response.data === undefined || error.response.data.message === undefined) {
    message = 'System Error';
  } else if (error.response.statusText === "Unauthorized") {
    window.location =`${window.location.origin}/#/login`
  } else {
    message = error.response.data.message
  }
  return message;
}
