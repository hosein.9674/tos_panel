import React from 'react';
import {

  Input,
  FormGroup,
  CardHeader,
  Card,
  CardBody,
  Button,
  InputGroup,
  InputGroupAddon,
  InputGroupText
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as cssConstant from "../../cssConstant";
import {PulseLoader} from 'react-spinners';
import SimpleReactValidator from "../../SimpleReactValidator";
import ErrorBoundary from '../../Errors/ErrorBoundary';
import {getErrorMessage} from "../../consts";
import '../styles.scss';
import questionsService from "../../../services/QuestionsService";
class newQuestion extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showErrors: false,
      messages: [],
      loading: false,
      category_id: '',
      course_id: '',
      correct_option:'',
      courses: [],
      categories: [],
      level: '',
      question:'',
      description: '',
      options1:'',
      options2:'',
      options3:'',
      options4:'',
      randomize: true,
      book_version: 0,
      for_game: true,
      for_exercise: true,
      has_answer: false
    }
    this.validator = new SimpleReactValidator();
    this.questionsService = new questionsService();
    this.commonChange = this.commonChange.bind(this);
    this.createQuestion = this.createQuestion.bind(this);

    this.fetchCourses();

  }

  commonChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
    if(event.target.name==='course_id'){
      this.fetchCategory(event.target.value);
    }
    console.log(event.target.name, ":" , event.target.value)
  }




  fetchCourses() {
    this.questionsService.getCoursesList(801).then(res => {

      this.setState({
        courses: res.data,
        loading: false
      });
    }).catch(error => {

      this.setState({loading: false,});
      this.setState({showErrors: true, messages:getErrorMessage(error)});
    });
  }


  fetchCategory(id) {
    this.questionsService.getCategoryList(id).then(res => {
      this.setState({
        categories: res.data,
        loading: false
      });
    }).catch(error => {
      this.setState({loading: false,});
      this.setState({showErrors: true, messages:getErrorMessage(error)});
    });
  }



  createQuestion() {
    if (this.validator.allValid()){
    this.setState({loading: true});
    this.questionsService.createQuestion({
      course_id: this.state.course_id,
      category_id: this.state.category_id,
      level: this.state.level,
      question: this.state.question,
      description: this.state.description,
      option1:this.state.option1,
      option2:this.state.option2,
      option3:this.state.option3,
      option4:this.state.option4,
      correct_option: this.state.correct_option,
      randomize: true,
      book_version: 0,
      for_game: true,
      for_exercise: true,
      has_answer: false
    }).then(res => {
        this.setState({loading: false});
        this.props.history.push("/questions")
      }
    ).catch(error => {
    
      this.setState({loading: false,});
      this.setState({showErrors: true, messages:getErrorMessage(error)});
    });}
    else{
      this.validator.showMessages();
      this.forceUpdate();
    }
  }


  render() {
    return (
      <div>

        <Card className="p-12">
          <div className='sweet-loading'>
            <PulseLoader
              css={cssConstant.override}
              sizeUnit={"px"}
              size={25}
              color={'#20a8d8'}
              loading={this.state.loading}
            />
          </div>


          <CardHeader>
            <strong>تعریف سوال</strong>
          </CardHeader>
          <CardBody>
            {this.state.showErrors && <ErrorBoundary messages={this.state.messages}/>}

            <div className='row'>


              <div className='col-md-6'>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="append">
                      <InputGroupText>انتخاب درس</InputGroupText>
                    </InputGroupAddon>
                    <Input type="select" name="course_id" id="course_id" onChange={this.commonChange}>
                      <option key={''} value="">انتخاب</option>
                      {
                        this.state.courses.map(item =>
                          <option key={item.id} value={item.id}>{item.title}</option>
                        )
                      }
                    </Input>
                   
                  </InputGroup>
                  {this.validator.message('انتخاب درس', this.state.course_id, 'required')}
                </FormGroup>
              </div>

              <div className='col-md-6'>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="append">
                      <InputGroupText>انتخاب فصل</InputGroupText>
                    </InputGroupAddon>
                    <Input disabled={!this.state.course_id} type="select" name="category_id" id="category_id" onChange={this.commonChange}>
                      <option key={''} value="">انتخاب</option>
                      {
                        this.state.categories.map(item =>
                          <option key={item.id} value={item.id}>{item.title}</option>
                        )
                      }
                    </Input>
                   
                  </InputGroup>
                  {this.validator.message('انتخاب فصل', this.state.category_id, 'required')}
                </FormGroup>
              </div>

            </div>


            <div className='row'>

              <div className='col-md-6'>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="append">
                      <InputGroupText>سطح سوال</InputGroupText>
                    </InputGroupAddon>
                    <Input type="select" name="level" id="level" onChange={this.commonChange}>
                      <option key={''} value="">انتخاب</option>
                      <option key={1} value={1}>{1}</option>
                      <option key={2} value={2}>{2}</option>
                      <option key={3} value={3}>{3}</option>
                    </Input>
                  </InputGroup>
                  {this.validator.message('سطح سوال', this.state.level, 'required')}
                </FormGroup>
              </div>


              <div className='col-md-6'>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="append">
                      <InputGroupText>صورت سوال</InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" name="question" onChange={this.commonChange} />
                  </InputGroup>
                  {this.validator.message('صورت سوال', this.state.question, 'required')}
                </FormGroup>
              </div>
            </div>

           

            <div className='row'>

<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText> گزینه اول</InputGroupText>
      </InputGroupAddon>
      <Input type="text" name="option1" onChange={this.commonChange} />
    </InputGroup>
    {this.validator.message('گزینه اول', this.state.option1, 'required')}
  </FormGroup>
</div>
<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText> گزینه دوم</InputGroupText>
      </InputGroupAddon>
      <Input type="text" name="option2" onChange={this.commonChange} />
    </InputGroup>
    {this.validator.message('گزینه دوم ', this.state.option2, 'required')}
  </FormGroup>
</div>
</div>

<div className='row'>

<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText> گزینه سوم</InputGroupText>
      </InputGroupAddon>
      <Input type="text" name="option3" onChange={this.commonChange} />
    </InputGroup>
    {this.validator.message(' گزینه سوم', this.state.option3, 'required')}
  </FormGroup>
</div>
<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText> گزینه چهارم</InputGroupText>
      </InputGroupAddon>
      <Input type="text" name="option4" onChange={this.commonChange} />
    </InputGroup>
    {this.validator.message('گزینه چهارم', this.state.option4, 'required')}
  </FormGroup>
</div>
</div>
<div className='row'>
              <div className='col-md-6'>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="append">
                      <InputGroupText>شرح سوال</InputGroupText>
                    </InputGroupAddon>
                    <Input type="textarea" name="description" id="description" onChange={this.commonChange} />
                  </InputGroup>
                  {this.validator.message('شرح سوال', this.state.description, 'required')}
                </FormGroup>
              </div>


              <div className='col-md-6'>
                <FormGroup>
                  <InputGroup>
                    <InputGroupAddon addonType="append">
                      <InputGroupText>گزینه صحیح</InputGroupText>
                    </InputGroupAddon>
                    <Input type="select" name="correct_option" id="correct_option" onChange={this.commonChange}>
                      <option key={''} value="">انتخاب</option>
                      <option key={1} value={1}>1گزینه</option>
                      <option key={2} value={2}>2گزینه</option>
                      <option key={3} value={3}>3گزینه</option>
                      <option key={4} value={4}>4گزینه</option>
                    </Input>
                  </InputGroup>
                  {this.validator.message('گزینه صحیح', this.state.correct_option, 'required')}
                </FormGroup>
              </div>

            </div>
            <Button color="primary" className="submit_btn" disabled={this.state.loading}
                    onClick={this.createQuestion}
                    active
                    tabIndex={-1}>ثبت </Button>
          </CardBody>
        </Card>
      </div>
    );
  }
}


export default newQuestion;
