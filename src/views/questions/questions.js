import React from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Input,
  FormGroup,
  ModalHeader,
Col,Row,
  InputGroup,
  InputGroupAddon,
  InputGroupText,Modal, ModalBody
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from 'react-router-dom';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import 'react-confirm-alert/src/react-confirm-alert.css'
import * as constant from "../consts";
import {PulseLoader} from "react-spinners";
import * as cssConstant from "../cssConstant";
import ErrorBoundary from '../Errors/ErrorBoundary';
import questionsService from "../../services/QuestionsService";
import {getErrorMessage} from "../consts";
import './styles.scss';

class questions extends React.Component {
  constructor(props) {

    super(props);
   
    this.questionsService = new questionsService();
    
    this.state = {
      showErrors: false,
      messages: [],
      loading:false,
      course_id:'',
      category_id:'',
      level:'',
      question:'',
      description:'',
      correct_option:'',
      options1:'',
      options2:'',
      options3:'',
      options4:'',
      modal:false,
      modal_course_id:'',
      modal_category_id:'',
      modal_level:'',
      modal_question:'',
      modal_description:'',
      modal_option1:'',
      modal_option2:'',
      modal_option3:'',
      modal_option4:'',
      modal_correct_option:'',
      columns: [{
        Header: ' شناسه درس',
        accessor: 'course_id',
        filterable: false
      },
        {
          Header: 'شناسه دسته بندی',
          accessor: 'category_id',
          filterable: false
        },
        {
        Header: 'سطح',
        accessor: 'level',
        filterable: false

      },
        {
          Header: ' صورت سوال',
          accessor: 'question',
          filterable: false
        }
        ,
        {
          Header: 'جواب صحیح',
          accessor: 'correct_option',
          filterable: false

        },{
           Header: 'عملیات',
          accessor: 'operation',
          filterable: false
          ,
          Cell: row =>

            <div >
              <span  title=" مشاهده جزئیات سوال" className='btn page-link-Edit' onClick={() => this.showQuestionDetail(row)} >
              مشاهده جزئیات
              </span>
            </div>,

        }],
    };
    this.fetchQuestions = this.fetchQuestions.bind(this);
    this.toggle= this.toggle.bind(this);
  }
   toggle (){
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
   }
  showQuestionDetail(e) {
    
      this.setState({
        loading: false,
        modal_course_id:e.original.course_id,
        modal_category_id:e.original.category_id,
        modal_level:e.original.level,
        modal_question:e.original.question,
        modal_description:e.original.description,
        modal_option1:e.original.option1,
        modal_option2:e.original.option2,
        modal_option3:e.original.option3,
        modal_option4:e.original.option4,
        modal_correct_option:e.original.correct_option,
        modal:true
      })
     
    }


  fetchQuestions(state, instance) {
    
    this.setState({loading: true});
    if (!state.page)
    state.page = 0;
    if (!state.pageSize)
      state.pageSize = 20;
    let data=[]
    this.questionsService.getQuestions(state.page).then(res => {
     
      // let userId=localStorage.getItem(constant.User_ID,);
        // if( userId ===res.date.creator_id ){
        
        
          for(let key of res.data.data){
            data.push({
              course_id: key.course_id,
              category_id: key.category_id,
              level: key.level,
              question: key.question,
              description: key.description,
              option1: key.option1,
              option2: key.option2,
              option3: key.option3,
              option4: key.option4,
              correct_option: key.correct_option, 
            })
        }
        
       
      this.setState({
        data: data,
        loading: false,
        pageSize: res.data.perPage,
        pages: res.data.lastPage,
      });
     
       
    
    }).catch(error => {
    
      this.setState({loading: false });
      this.setState({showErrors: true, messages:getErrorMessage(error)});
    });
  }
  showDetailModal() {
  
    return (
     <Modal isOpen={this.state.modal} toggle={this.toggle} >
          <ModalHeader toggle={this.toggle}> </ModalHeader>
        <ModalBody>
        <div className='row'>


<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText> شناسه درس</InputGroupText>
      </InputGroupAddon>
      <Input type="text" name="modal_course_id" readOnly defaultValue={this.state.modal_course_id}/>
    </InputGroup>
  </FormGroup>
</div>

<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText>شناسه فصل</InputGroupText>
      </InputGroupAddon>
      <Input type="text" name="modal_category_id" readOnly defaultValue={this.state.modal_category_id}/>
    </InputGroup>
   
  </FormGroup>
</div>

</div>

<div className='row'>

<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText>سطح سوال</InputGroupText>
      </InputGroupAddon>
      <Input type="text" name="modal_level"  readOnly defaultValue={this.state.modal_level}/>
      
    </InputGroup>
  </FormGroup>
</div>


<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText>صورت سوال</InputGroupText>
      </InputGroupAddon>
      <Input type="text" name="modal_question" readOnly defaultValue={this.state.modal_question}/>
     
    </InputGroup>
   
  </FormGroup>
</div>
</div>

<div className='row'>

<div className='col-md-6'>
<FormGroup>
<InputGroup>
<InputGroupAddon addonType="append">
<InputGroupText> گزینه اول</InputGroupText>
</InputGroupAddon>
<Input type="text" name="modal_option1"  readOnly defaultValue={this.state.modal_option1}/>
</InputGroup>
</FormGroup>
</div>
<div className='col-md-6'>
<FormGroup>
<InputGroup>
<InputGroupAddon addonType="append">
<InputGroupText> گزینه دوم</InputGroupText>
</InputGroupAddon>
<Input type="text" name="modal_option2" readOnly defaultValue={this.state.modal_option2}/>
</InputGroup>
</FormGroup>
</div>
</div>

<div className='row'>

<div className='col-md-6'>
<FormGroup>
<InputGroup>
<InputGroupAddon addonType="append">
<InputGroupText> گزینه سوم</InputGroupText>
</InputGroupAddon>
<Input type="text" name="modal_option3" readOnly defaultValue={this.state.modal_option3}/>
</InputGroup>
</FormGroup>
</div>
<div className='col-md-6'>
<FormGroup>
<InputGroup>
<InputGroupAddon addonType="append">
<InputGroupText> گزینه چهارم</InputGroupText>
</InputGroupAddon>
<Input type="text" name="modal_option4" readOnly defaultValue={this.state.modal_option4}/>
</InputGroup>
</FormGroup>
</div>
</div>
<div className='row'>
<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText>شرح سوال</InputGroupText>
      </InputGroupAddon>
      <Input type="textarea" name="modal_description" readOnly defaultValue={this.state.modal_description}/>
    </InputGroup>
  
  </FormGroup>
</div>


<div className='col-md-6'>
  <FormGroup>
    <InputGroup>
      <InputGroupAddon addonType="append">
        <InputGroupText>گزینه صحیح</InputGroupText>
      </InputGroupAddon>
      <Input type="text" name="modal_correct_option" readOnly defaultValue={this.state.modal_correct_option}/>
    
    </InputGroup>
  </FormGroup>
</div>

</div>

        </ModalBody>
  
      </Modal>
   
    );
}

  render() {
    const {data, pages, pageSize} = this.state;
    return (
      <div className="App">
        <div className='sweet-loading'>
          <PulseLoader
            css={cssConstant.override}
            sizeUnit={"px"}
            size={25}
            color={'#20a8d8'}
            loading={this.state.loading}
          />
        </div>
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" lg="12">
              <Card>
                <CardHeader>
                  لیست  سوالات
                  <span className='newQuestion-link'>
                       <Link to="/newQuestion">
                    <i className="fa fa-plus fa-lg"></i><span className="newTitr">ایجاد  سوال جدید </span>
                  </Link>
                  </span>
                </CardHeader>
                <CardBody>
                  {this.state.showErrors && <ErrorBoundary messages={this.state.messages}/>}
                  <ReactTable
                    columns={[
                      {
                        columns: this.state.columns
                      }
                    ]}
                    manual
                    data={data}
                    pages={pages}
                    pageSize={pageSize}
                    noDataText=""
                    onFetchData={this.fetchQuestions}
                    defaultPageSize={20}
                    pageSizeOptions={[20]}
                    className="-striped -highlight"
                    previousText={constant.Next}
                    nextText={constant.Previous}
                    filterable
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
     {this.state.modal?this.showDetailModal():''}
      </div>
    );
  }
}


export default questions;
