import React, {Component} from 'react';
import {Alert} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
class ErrorBoundary extends Component {

  
  render() {
    
    return(
      <div>
     
            <Alert color="danger" key={this.props.messages}>
             {this.props.messages}
            </Alert>
       
      </div>
    )
  }
}




export default ErrorBoundary;
