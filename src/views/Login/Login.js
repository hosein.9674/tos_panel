import React, {Component} from 'react';
import {
  Button,
  Card,
  CardGroup,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AuthenticationService from "../../services/AuthenticationService";
import * as constant from "../consts";
import ErrorBoundary from '../Errors/ErrorBoundary';
import SimpleReactValidator from "../SimpleReactValidator";
import {getErrorMessage} from "../consts";
import {PulseLoader} from "react-spinners";
import * as cssConstant from "../cssConstant";
import './styles.scss';


class Login extends Component {

  constructor(props) {
    const queryString = require('query-string');
    super(props);
    this.validator = new SimpleReactValidator();
    this.authenticatioService = new AuthenticationService();
    const history = queryString.parse(this.props.location.search, {ignoreQueryPrefix: true}).redirect;


    this.state = {
      showErrors: false,
      messages: [],
      identifier: '',
      message: '',
      loading: false,
      username: '',
      password: '',
      history: history,
      value: '',

    };
    this.commonChange = this.commonChange.bind(this);
    this.loginUser = this.loginUser.bind(this);

  }

  commonChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
    this.setState({
      showErrors: false
    });
  }

  loginUser() {
    if (this.validator.allValid()){
      this.setState({loading: true});
      this.authenticatioService.login(this.state.username, this.state.password).then(e => {

        localStorage.setItem(constant.TOKEN, e.data.token);
        this.props.history.push("/dashboard");
        this.setState({loading: false});
      }).catch(error => {
        this.setState({loading: false});
        this.setState({showErrors: true, messages:getErrorMessage(error)});

      })
    }else {
      this.validator.showMessages();
      this.forceUpdate();
    }

  }



  render() {
    return (
      <div className="app flex-row align-items-center">
        <div className='sweet-loading'>
          <PulseLoader
            css={cssConstant.override}
            sizeUnit={"px"}
            size={25}
            color={'#20a8d8'}
            loading={this.state.loading}
          />
        </div>
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  {this.state.showErrors && <ErrorBoundary messages={this.state.messages}/>}
                  <div>
                    <h1>صفحه ورود</h1>
                    <p className="text-muted">برای ورود کلیک کنید</p>
                    <InputGroup className="mb-3">

                      <Input type="text" placeholder="نام کاربری" name="username"
                             onChange={this.commonChange}
                      />

                    </InputGroup>
                    {this.validator.message('نام کاربری', this.state.username, 'required')}
                    <InputGroup className="mb-4">

                      <Input type="password" placeholder="رمز عبور" name="password"
                             onChange={this.commonChange}/>

                    </InputGroup>
                    {this.validator.message('رمز عبور', this.state.password, 'required')}
                    <Row  >
                      <Col xs="6">
                        <Button color="primary" className=""
                                onClick={this.loginUser}>ورود</Button>
                      </Col>

                    </Row>
                  </div>
                </Card>

              </CardGroup>
            </Col>

          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
