import React, {Component} from 'react';
import { DropdownMenu, DropdownToggle,
  DropdownItem, Dropdown}from 'reactstrap';
  import 'bootstrap/dist/css/bootstrap.min.css';




class DefaultHeader extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      
      dropdownOpen: false,

    }
    this.toggleDropdown = this.toggleDropdown.bind(this);
   
  }

  toggleDropdown() {
    this.setState({
        dropdownOpen: !this.state.dropdownOpen
    });
}


  render() {
    

    return (
      <React.Fragment>  

        <div className='panel-header'>
        <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown}>
                                            <DropdownToggle nav caret>
                                            <img 
                                            alt=""
                                            src="../../assets/img/avatars/6.jpg" className="img-avatar "/>
                                            </DropdownToggle>
                                            <DropdownMenu>
                                                <DropdownItem className="signOut" onClick={e => this.props.onLogout(e)}>
                                                <i className="fa fa-lock"></i>خروج</DropdownItem>
                                            </DropdownMenu>
                                        </Dropdown>
        
        </div>
      </React.Fragment>
    );
  }
}


export default DefaultHeader;
