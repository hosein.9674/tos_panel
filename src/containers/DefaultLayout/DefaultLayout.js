import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import {Link} from 'react-router-dom';
import { Container } from 'reactstrap';
import routes from '../../routes';
import * as constant from '../../views/consts';
import 'bootstrap/dist/css/bootstrap.min.css';
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {
 
  loading = () => <div className="animated fadeIn m-auto pt-1 text-center">Loading...</div>

  signOut(e) {
    e.preventDefault();
    localStorage.removeItem(constant.TOKEN);
    this.props.history.push('/login')
    //  window.location.reload()
  }




  render() {
   
    
    return (
      <div className="app">
        
    
   
          <Suspense  fallback={this.loading()}> 
            <DefaultHeader onLogout={e=>this.signOut(e)}/>
           </Suspense> 
         
        <div className="app-body">
         
           
            <Suspense>
            <div className="sidenav">
           
            <Link  to="/questions">لیست سوالات</Link>
           
            <Link  to="/dashboard">مشاهده نمودار</Link>
           
          </div>
          </Suspense>
           
           
          <main className="main">
           
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                  
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                          <route.component {...props} />
                        )} />
                    ) : (null);
                  })}
                  
                  
                  <Redirect from="/" to="/dashboard" />
                </Switch>
              </Suspense>
            </Container>
          </main>
         
        </div>
        
      </div>
    );
  }
}

export default DefaultLayout;
