import React from 'react';
const Dashboard = React.lazy(() => import('./views/Dashboard/Dashboard'));
const newQuestion = React.lazy(() => import('./views/questions/newQuestion/newQuestion'));
const login = React.lazy(() => import('./views/Login/Login'));
const questions = React.lazy(() => import('./views/questions/questions'));
const ErrorBoundary = React.lazy(() => import('./views/Errors/ErrorBoundary'));



const routes = [

  {path: '/dashboard', name: 'Dashboard', component: Dashboard},
  {path: '/questions', name: 'لیست سوالات', component: questions},
  {path: '/newQuestion', name: 'ایجاد سوال جدید ', component: newQuestion},
  {path: '/login', name: 'ورود', component: login},
  {path: '/ErrorBoundary', name: 'صفحه ی خطا ', component: ErrorBoundary},


];

export default routes;
