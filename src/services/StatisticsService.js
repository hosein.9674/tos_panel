import * as constant from "../views/consts";
import axios from "axios/index";

export default class StatisticsService {

  async getStatistics() {
    let res= await axios.get(`${constant.ApiUrl}/admin/questions/stats`, this.getOptions())
     return res;
     }
   
  


  getOptions() {
    return {
      headers: this.getHeaders()
    };
  }

  getHeaders() {
    let token = localStorage.getItem(constant.TOKEN);
    return {
      "Authorization":'Bearer '+ token
    }
  }
}
