import * as constant from "../views/consts";
import axios from "axios/index";

export default class AuthenticationService {

 async login(username, password) {
    const res = await axios.post(constant.Login, {username: username, password: password});
    return res;
  }
}
