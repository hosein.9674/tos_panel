import * as constant from "../views/consts";
import axios from "axios/index";

export default class  questionsService {





  async createQuestion(request) {
    let res =await axios.post(`${constant.ApiUrl}/admin/questions`, request, this.getOptions())
    return res;
  }


async getQuestions(page) {
 let res= await axios.get(`${constant.ApiUrl}/admin/questions`,{params: { pagination:1,page:page+1},headers: this.getHeaders()})
  return res;
  }


  
  async getCoursesList(id) {
    let res =await axios.get(`${constant.ApiUrl}/admin/stages/${id}/courses`,{ headers: this.getHeaders()})
    return res;
  }

  async getCategoryList(id) {
    let res =await axios.get(`${constant.ApiUrl}/admin/courses/${id}/categories`,{ headers: this.getHeaders()})
    return res;
  }



  getOptions() {
    return {
      headers: this.getHeaders()
    };
  }

  getHeaders() {
    let token = localStorage.getItem(constant.TOKEN);
    return {
      "Authorization":'Bearer '+ token
    }
  }
}
